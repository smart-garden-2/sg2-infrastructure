= Backend

Docker-compose config for running an InfluxDB instance, a Moquitto broker and a verticle that acts as a bridge.

InfluxDB dashboard available at http://localhost:8086.
ngrok dashboard available at http://localhost:4040.

For the docker-compose stack to work, it is necessary to create a `.env` file in the root of the project. See `env.template` for an example.
