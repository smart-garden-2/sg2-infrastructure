import random
from influxdb_client import InfluxDBClient, WriteOptions
from datetime import timedelta, datetime, date, time

bucket = "smart_garden"
measurements = ["soilHumidity", "airHumidity", "temperature", "pressure", "tankLevel"]

client = InfluxDBClient.from_config_file("influxdb_config.ini")
write_api = client.write_api(
    write_options=WriteOptions(batch_size=10_000, flush_interval=2_000)
)

mu = 100
sigma = 50
delta = timedelta(hours=1)
start_datetime = datetime.combine(date(2020, 2, 22), time(21))


def get_random_data():
    return random.gauss(mu, sigma)


def gen_timeseries():
    dt = start_datetime
    while dt - start_datetime != timedelta(days=365):
        yield dt
        dt += delta


def gen_measurement(timestamp_data):
    for item in measurements:
        yield get_line_protocol_data({
          "measure_name": item,
          "time": timestamp_data,
          "value": get_random_data()
        })


def get_line_protocol_data(row):
    measure_name = row["measure_name"]
    value = row["value"]
    timestamp_iso = row["time"].timestamp() * 1e9
    return f"{measure_name} {measure_name}={int(value)} {int(timestamp_iso)}"


def on_exit():
    write_api.close()
    client.close()


if __name__ == "__main__":
    try:
        for timestamp in gen_timeseries():
            for measurement in gen_measurement(timestamp):
                write_api.write(bucket=bucket, record=measurement)
        on_exit()
    except KeyboardInterrupt:
        on_exit()
